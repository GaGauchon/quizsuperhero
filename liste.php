<!DOCTYPE html>
<html>
<head>
    <title>Voici tous les joueurs</title>
    <link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>

<table>
    <tr>
        <th>ID</th>
        <th>Nom</th>
        <th>Prenom</th>
        <th>Mot de passe</th>
        <th>Pseudo</th>
    </tr>

    <?php


    //Connexion à la base de donnees

    $bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'root','');

    //Recuperation des donnees de la table utilisateur

    $reponse = $bdd->query('SELECT * FROM utilisateur');

    // Boucle pour accéder à tous les utilisteurs

    while ($donnees = $reponse->fetch())

        //Affichage des donnees de chaque utilisateur
    {
        echo "<tr>";
        echo "<td>" .$donnees['id']. "</td>";
        echo "<td>" .$donnees['nom']."</td>";
        echo "<td>" .$donnees['prenom']."</td>";
        echo "<td>" .$donnees['motdepasse']. "</td>";
        echo "<td>" .$donnees['pseudo']."</td>";
        echo "</tr>";
    }


    // Necessaire pour indiquer qu'on à terminé le traitement des donnees
    $reponse->closeCursor();


    ?>
</table>

</body>
</html>

